/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package masino_2;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

/*
        This class create a GUI that displays different images on cards with labels
        describing some image properties and buttons to display the filters
        as well as hotkeys that activate the buttons
        Also many of my commits are linked to other branches and I cannot for the
        life of me get it to merge correctly so I promise I do have commits they
        are just hiding
 */
public class Main extends JFrame {

    // Create the cardlayout
    private CardLayout cl;
    // Create the button panel
    private JPanel buttonPanel;
    // Create the card panel
    private JPanel cardPanel;
    // Create the buffered image orignal picture
    private BufferedImage originalPic;
    // Create the info panel
    private JPanel infoPanel;
    // Create the buffered image to track the current image
    private BufferedImage currentImage;
    // Create a label for the RGb
    private JLabel RGB;
    // Create a label for the resolution
    private JLabel res;
    // Create a label for the name
    private JLabel name;
    // Create a JButton for the flipped image
    private JButton flipped;
    // Create a JButton for the grey image
    private JButton grey;
    // Create a JButton for the open image
    private JButton open;
    // Create a JButton for the save image
    private JButton save;
    // Create a JButton for the inverted image
    private JButton inverted;
    // Create a JButton for the original image
    private JButton original;
    // Create a JButton for the noise image
    private JButton noise;

    /*
       Constructor for the main
     */
    public Main() throws IOException {
        // Set the title of the JFrame
        setTitle("Card Layout Example");

        // Set visibility of the JFrame
        setSize(300, 150);

        // Create a JPanel to hold cards
        cardPanel = new JPanel();

        // Initialize cl, a cardlayout
        cl = new CardLayout();

        // set the layout of the cardpanel to the cardlayout
        cardPanel.setLayout(cl);

        // Create a JPanel for buttons
        buttonPanel = new JPanel();
        // Create a JPanel for the image information
        infoPanel = new JPanel();

        // Credit: https://www.javatpoint.com/java-jbutton
        // button to open open dialog
        open = new JButton("Load (L)");
        // Button to open the save dialog
        // Disable it to begin with to reduce error chances
        save = new JButton("Save (S)");
        save.setEnabled(false);
        // Button to display the original image
        // Disable it to begin with to reduce error chances
        original = new JButton("Original (O)");
        original.setEnabled(false);
        // Button to display the inverted image
        // Disable it to begin with to reduce error chances
        inverted = new JButton("Inverted (I)");
        inverted.setEnabled(false);
        // Button to display the greyscaled image
        // Disable it to begin with to reduce error chances
        grey = new JButton("Grey (G)");
        grey.setEnabled(false);
        // Button to display the flipped image
        // Disable it to begin with to reduce error chances
        flipped = new JButton("Flipped (F)");
        flipped.setEnabled(false);
        // Button to display the toNoise image
        // Disable it to begin with to reduce error chances
        noise = new JButton("Noise (P)");
        noise.setEnabled(false);

        // JLabel to display the resolution of the image
        res = new JLabel("");
        // Add the JLabel to the infoPanel
        infoPanel.add(res);
        // Make it invisible as there is no image to describe at the start
        res.setVisible(false);

        // JLabel to display the average RGB value of the pixels
        RGB = new JLabel("");
        // Add the JLabel to the infoPanel
        infoPanel.add(RGB);
        // Make it invisible as there is no image to describe at the start
        RGB.setVisible(false);

        // JLabel to display the name of the image
        name = new JLabel("");
        // Add the JLabel to the infoPanel
        infoPanel.add(name);
        // Make it invisible as there is no image to describe at the start
        name.setVisible(false);

        // An action which calls the opened method
        Action openFile = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                opened();
            }
        };
        // Adds a keybinding to the cardPanel for L which calls the openfile action
        // WHEN_IN_FOCUSED_WINDOW used to ensure that the focus does not interfere 
        // with the keybinding
        // Credit: https://docs.oracle.com/javase/tutorial/uiswing/misc/keybinding.html
        cardPanel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("L"),
                "open");
        // Add the action to the cardPanel
        cardPanel.getActionMap().put("open",
                openFile);

        // An action that calls the saveFile method
        // Credit: https://docs.oracle.com/javase/tutorial/uiswing/misc/keybinding.html
        Action saveFile = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // A try catch block to ensure that a null file does not interfere
                try {
                    saved();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        // Adds a keybinding to the cardPanel for S which calls the saveFile action
        // WHEN_IN_FOCUSED_WINDOW used to ensure that the focus does not interfere 
        // with the keybinding
        // Credit: https://docs.oracle.com/javase/tutorial/uiswing/misc/keybinding.html
        cardPanel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("S"),
                "save");
        // Add the action to the cardPanel
        cardPanel.getActionMap().put("save",
                saveFile);

        // An action that calls the displays the original image card
        // Credit: https://docs.oracle.com/javase/tutorial/uiswing/misc/keybinding.html
        Action originalKey = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Displays the original card on the cardPanel
                cl.show(cardPanel, "original");
            }
        };
        // Adds a keybinding to the cardPanel for O which calls the originalKey action
        // WHEN_IN_FOCUSED_WINDOW used to ensure that the focus does not interfere 
        // with the keybinding
        // Credit: https://docs.oracle.com/javase/tutorial/uiswing/misc/keybinding.html
        cardPanel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("O"),
                "orig");
        // Adds the action to the cardPanel
        cardPanel.getActionMap().put("orig",
                originalKey);

        // An action that calls the makeInverted method
        // Credit: https://docs.oracle.com/javase/tutorial/uiswing/misc/keybinding.html
        Action invertedKey = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Try catch block to ensure a null file does not interfere
                try {
                    makeInverted();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        // Adds a keybinding to the cardPanel for I which calls the invertedKey action
        // WHEN_IN_FOCUSED_WINDOW used to ensure that the focus does not interfere 
        // with the keybinding
        // Credit: https://docs.oracle.com/javase/tutorial/uiswing/misc/keybinding.html
        cardPanel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("I"),
                "invert");
        // Adds the action to the cardPanel
        cardPanel.getActionMap().put("invert",
                invertedKey);

        // An action that calls the makeGrey method
        // Credit: https://docs.oracle.com/javase/tutorial/uiswing/misc/keybinding.html
        Action greyKey = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Try catch block to ensure a null file does not interfere
                try {
                    makeGrey();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        // Adds a keybinding to the cardPanel for G which calls the greyKey action
        // WHEN_IN_FOCUSED_WINDOW used to ensure that the focus does not interfere 
        // with the keybinding
        // Credit: https://docs.oracle.com/javase/tutorial/uiswing/misc/keybinding.html
        cardPanel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("G"),
                "grey");
        // Adds the action to the cardPanel
        cardPanel.getActionMap().put("grey",
                greyKey);

        // An action that calls the makeFlip method
        // Credit: https://docs.oracle.com/javase/tutorial/uiswing/misc/keybinding.html
        Action flippedKey = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Try catch block to ensure a null file does not interfere
                try {
                    makeFlip();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        // Adds a keybinding to the cardPanel for F which calls the flippedKey action
        // WHEN_IN_FOCUSED_WINDOW used to ensure that the focus does not interfere 
        // with the keybinding
        // Credit: https://docs.oracle.com/javase/tutorial/uiswing/misc/keybinding.html
        cardPanel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("F"),
                "flip");
        // Adds the action to the cardPanel
        cardPanel.getActionMap().put("flip",
                flippedKey);

        // An action that calls the makeFlip method
        // Credit: https://docs.oracle.com/javase/tutorial/uiswing/misc/keybinding.html
        Action noiseKey = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Try catch block to ensure a null file does not interfere
                try {
                    toNoise();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        // Adds a keybinding to the cardPanel for N which calls the noiseKey action
        // WHEN_IN_FOCUSED_WINDOW used to ensure that the focus does not interfere 
        // with the keybinding
        // Credit: https://docs.oracle.com/javase/tutorial/uiswing/misc/keybinding.html
        cardPanel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("N"),
                "noise");
        // Adds the action to the cardPanel
        cardPanel.getActionMap().put("noise",
                noiseKey);

        // Adds the save button to the buttonPanel
        buttonPanel.add(save);
        // Adds the open button to the buttonPanel
        buttonPanel.add(open);
        // Adds the original button to the buttonPanel
        buttonPanel.add(original);
        // Adds the inverted button to the buttonPanel
        buttonPanel.add(inverted);
        // Adds the grey button to the buttonPanel
        buttonPanel.add(grey);
        // Adds the flipped button to the buttonPanel
        buttonPanel.add(flipped);
        // Adds the toNoise button to the buttonPanel
        buttonPanel.add(noise);

        // An actionListener for the open button
        // Inspired by class notes
        open.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                // Calls the opened method
                opened();
            }
        });

        // An actionListener for the save button
        // Inspired by class notes
        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                // Try catch block to ensure null files do not interfere
                try {
                    // Calls the saved method
                    saved();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        // An actionListener for the original button
        // Inspired by class notes
        original.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                // Sets currentImage to OriginalPic
                currentImage = originalPic;
                // Display the original card
                cl.show(cardPanel, "original");
            }
        });

        // An actionListener for the inverted button
        // Inspired by class notes
        inverted.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                // Try catch block to ensure a null file does not interfere
                try {
                    // Call the makeInverted method
                    makeInverted();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        // An actionListener for the grey button
        // Inspired by class notes
        grey.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                // Try catch block to ensure a null file does not interfere
                try {
                    // Calls makeGrey method
                    makeGrey();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        // An actionListener for the flipped button
        // Inspired by class notes
        flipped.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                // Try catch block to ensure a null file does not interfere
                try {
                    // Calls makeFlip method
                    makeFlip();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        // An actionListener for the toNoise button
        // Inspired by class notes
        noise.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                // Try catch block to ensure a null file does not interfere
                try {
                    // Calls the toNoise method
                    toNoise();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        // Credit: https://www.geeksforgeeks.org/java-awt-cardlayout-class/
        // Add the cardPanel to the Center
        getContentPane().add(cardPanel, BorderLayout.CENTER);
        // Add the buttonPanel to the South
        getContentPane().add(buttonPanel, BorderLayout.SOUTH);
        // Add the infoPanel to the North
        getContentPane().add(infoPanel, BorderLayout.NORTH);
    }

    /*
     A method that creates a copy of the original picture for manipulation
     @Return BufferedImage of the copy
     // Credit: https://stackoverflow.com/questions/3514158/how-do-you-clone-a-bufferedimage
     */
    public BufferedImage createCopy() {
        // Create a color model of the original image
        ColorModel cm = originalPic.getColorModel();
        // This checks if the image has already been masked
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        // This allows us to manipulate the pixels more
        WritableRaster raster = originalPic.copyData(null);
        // Create a new bufferedImage to manipulate
        BufferedImage copy = new BufferedImage(cm, raster, isAlphaPremultiplied, null);
        return copy;
    }

    /*
     A method that finds the average RGB values of all the pixels in the image
     @return the string of averages formatted to two decimal places
     // Credit: https://www.tutorialspoint.com/how-to-get-pixels-rgb-values-of-an-image-using-java-opencv-library
     */
    public String getRGB() throws IOException {
        // Set img to the currentImage being displayed
        BufferedImage img = currentImage;
        // Initialize the total number of red, green, and blue to 0
        int totred = 0;
        int totgreen = 0;
        int totblue = 0;
        // For every pixel in the Y dimension
        for (int y = 0; y < img.getHeight(); y++) {
            // For every pixel in the X direction
            for (int x = 0; x < img.getWidth(); x++) {
                // Retrieve the contents of a pixel
                int pixel = img.getRGB(x, y);
                // Create a Color object from the pixel value
                Color color = new Color(pixel, true);
                //Retrieve the R G B values
                double red = color.getRed();
                double green = color.getGreen();
                double blue = color.getBlue();
                // Add the values to each total counter
                totred += red;
                totgreen += green;
                totblue += blue;
            }
        }
        // Multiply to get the total number of pixels in the image
        double totpix = img.getHeight() * img.getWidth();
        // Divide the amount of red by the total number of pixels
        // Format the double to 2 decimal places for readability 
        // Credit: https://stackoverflow.com/questions/153724/how-to-round-a-number-to-n-decimal-places-in-java
        double avgred = totred / totpix;
        double red = (double) Math.round(avgred * 100d) / 100d;
        // Divide the amount of green by the total number of pixels
        // Format the double to 2 decimal places for readability 
        double avggreen = totgreen / totpix;
        double green = (double) Math.round(avggreen * 100d) / 100d;
        // Divide the amount of blue by the total number of pixels
        // Format the double to 2 decimal places for readability 
        double avgblue = totblue / totpix;
        double blue = (double) Math.round(avgblue * 100d) / 100d;
        // Return those formatted values in a string that labels them
        return ("Average pixel RGB values: " + " Red: " + red + " Green: " + green + " Blue: " + blue);
    }

    /*
     A method that gets the resolution of the image
     @Return a String of the resolution
     //Credit: https://stackoverflow.com/questions/672916/how-to-get-image-height-and-width-using-java
     */
    public String getResolution() {
        // set imgred to the current image being displayed
        BufferedImage imgres = currentImage;
        // Get the height of the image
        int height = imgres.getHeight();
        // Gethe the width of the image
        int width = imgres.getWidth();
        // Multiply the height by the width
        int rescalc = height * width;
        // return the calculation of resolution in string 
        return "" + rescalc;
    }

    /*
     A method that opens a file from the computer
     Credit: https://www.geeksforgeeks.org/java-swing-jfilechooser/#:~:text=JFileChooser%20is%20a%20part%20of%20java%20Swing%20package.&text=JFC%20contains%20many%20features%20that,a%20file%20or%20a%20directory%20
     */
    public void opened() {
        // Create a new fileChooser  starting in the home directory
        JFileChooser j = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
        // Invoke the showsOpenDialog function to show the save dialog
        int r = j.showOpenDialog(null);
        // If the user selects a file
        if (r == JFileChooser.APPROVE_OPTION) {
            try {
                // set original picture to the read in image 
                originalPic = ImageIO.read(new File(j.getSelectedFile().getAbsolutePath()));
                // create a new label with an imageIcon containing the original image
                // Credit: https://stackoverflow.com/questions/299495/how-to-add-an-image-to-a-jpanel#:~:text=Try%20this%3A,myPicture
                JLabel originalLabel = new JLabel(new ImageIcon(originalPic));
                // Create a new panel to hold the original image
                JPanel OPanel = new JPanel();
                OPanel.add(originalLabel);
                // Add the panel to the cardPanel
                cardPanel.add(OPanel, "original");
                // Display the original image card
                cl.show(cardPanel, "original");
                // Set currentImage to originalPic
                currentImage = originalPic;
                // Enable the previously disabled buttons
                // Credit: https://www.javatpoint.com/java-jbutton
                original.setEnabled(true);
                inverted.setEnabled(true);
                grey.setEnabled(true);
                flipped.setEnabled(true);
                save.setEnabled(true);
                noise.setEnabled(true);
                // Set the text of the RGB label and make it visible
                RGB.setText(getRGB());
                RGB.setVisible(true);
                // Set the text of the resolution label and make it visible
                res.setText("Resolution: " + getResolution());
                res.setVisible(true);
                // Set the text of the name label and make it visible
                name.setText("Name: " + j.getSelectedFile().getAbsolutePath());
                name.setVisible(true);
                // Revalidate the cardPanel to ensure the image displays
                cardPanel.revalidate();
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /*
     A method that saves your image to the computer 
    Credit: https://www.codejava.net/java-se/swing/show-save-file-dialog-using-jfilechooser
     */
    public void saved() throws IOException {
        // Create a new fileChooser
        JFileChooser fileChooser = new JFileChooser();
        // Filter the fileChooser with pngs
        fileChooser.setFileFilter(new FileNameExtensionFilter("*.png", "png"));
        // If the user selects the save option in the fileChooser
        if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            // Create a file of the selected file
            File file = fileChooser.getSelectedFile();
            // If that file already exists and is not a directory
            // Credit: https://stackoverflow.com/questions/1816673/how-do-i-check-if-a-file-exists-in-java
            if (file.exists() && !file.isDirectory()) {
                // Pop up a warning window that the old file will be overwritten
                // Credit: https://stackoverflow.com/questions/8581215/jfilechooser-and-checking-for-overwrite
                int response = JOptionPane.showConfirmDialog(this,
                        "The file " + file.getName()
                        + " already exists. Do you want to replace the existing file?",
                        "Ovewrite file", JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE);
                // If the user wants to continue and overwrite the file
                if (response == JOptionPane.YES_OPTION) {
                    // Save the current image to the computer
                    ImageIO.write(currentImage, "png", new File(file.getAbsolutePath()));
                }
            } else {
            }
        } else {
        }
    }

    /*
     A method that creates an image with inverted colors
    // Credit: https://dyclassroom.com/image-processing-project/how-to-convert-a-color-image-into-negative
     */
    public void makeInverted() throws IOException {
        // Create an invertedcopy with the createCopy method
        BufferedImage invertedcopy = createCopy();
        // For every pixel in the y dimension
        for (int y = 0; y < invertedcopy.getHeight(); y++) {
            // For every pixel in the x duimension
            for (int x = 0; x < invertedcopy.getWidth(); x++) {
                // Get the RGB values of the pixel
                int p = invertedcopy.getRGB(x, y);
                // Shift the byte 24 places and perform bitwise
                int a = (p >> 24) & 0xff;
                // Shift the byte 16 places and perform bitwise
                int r = (p >> 16) & 0xff;
                // Shift the byte 8 places and perform bitwise
                int G = (p >> 8) & 0xff;
                int b = p & 0xff;
                // Subtract the RGB from 255 to get the inverse
                r = 255 - r;
                G = 255 - G;
                b = 255 - b;
                // Set the new RGB values for the pixel
                p = (a << 24) | (r << 16) | (G << 8) | b;
                // Set that pixel in the copy to the calculated color
                invertedcopy.setRGB(x, y, p);
            }
        }
        // Create a new JLabel of an imageIcon containing the inverted copy
        // Credit: https://stackoverflow.com/questions/299495/how-to-add-an-image-to-a-jpanel#:~:text=Try%20this%3A,myPicture
        JLabel copiedlabel = new JLabel(new ImageIcon(invertedcopy));
        // Create a new JPanel and add the label to it
        JPanel IPanel = new JPanel();
        IPanel.add(copiedlabel);
        // Add the panel to the cardPanel
        cardPanel.add(IPanel, "invert");
        // Reset current image to the inverted image
        currentImage = invertedcopy;
        // Recalculate the RGB averages with getRGB and update the label
        RGB.setText(getRGB());
        // Display the inverted card
        cl.show(cardPanel, "invert");
    }

    /*
     A method that makes the image greyscaled
     Credit: https://gist.github.com/boolean444/899d6fa2b18f6b8f0f56bf98ad15d5a8
     */
    public void makeGrey() throws IOException {
        // Call the create invertedcopy method
        BufferedImage greyimg = createCopy();
        // For every pixel in the y dimension
        for (int i = 0; i < greyimg.getHeight(); i++) {
            // For every pixel in the x dimension
            for (int j = 0; j < greyimg.getWidth(); j++) {
                // Create a new color using the red green and blue respectively
                float r = new Color(greyimg.getRGB(j, i)).getRed();
                float g = new Color(greyimg.getRGB(j, i)).getGreen();
                float b = new Color(greyimg.getRGB(j, i)).getBlue();
                // Find the equivalent grey value for that pixel
                int grayScaled = (int) (r + g + b) / 3;
                // Change the color of that pixel to a color of the calculated grey
                greyimg.setRGB(j, i, new Color(grayScaled, grayScaled, grayScaled).getRGB());
            }
        }
        // Create a new label of the grey image in an imageIcon
        // Credit: https://stackoverflow.com/questions/299495/how-to-add-an-image-to-a-jpanel#:~:text=Try%20this%3A,myPicture
        JLabel greylabel = new JLabel(new ImageIcon(greyimg));
        // Create a panel for it and add the label to the panel
        JPanel GPanel = new JPanel();
        GPanel.add(greylabel);
        // add the panel to the cardpanel
        cardPanel.add(GPanel, "grey");
        // Reset current image to the grey image
        currentImage = greyimg;
        // Recalculate the average RGN values with getRGB and refresh the label
        RGB.setText(getRGB());
        // Display the image on the card
        cl.show(cardPanel, "grey");
    }

    /*
     A method that flips the image over the y axis
    // Credit: http://www.java2s.com/Tutorial/Java/0261__2D-Graphics/Fliptheimagehorizontally.htm
     */
    public void makeFlip() throws IOException {
        // Call the create invertedcopy method
        BufferedImage flip = createCopy();
        // This gets the inverse transformation of an image   
        AffineTransform tx = AffineTransform.getScaleInstance(-1, 1);
        tx.translate(-flip.getWidth(null), 0);
        AffineTransformOp op = new AffineTransformOp(tx,
                AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
        // This sets a new bufferdImage to the flipped version we just made
        BufferedImage imagep = op.filter(flip, null);
        // Adds the fipped image to a JLabel and ImageIcon
        // Credit: https://stackoverflow.com/questions/299495/how-to-add-an-image-to-a-jpanel#:~:text=Try%20this%3A,myPicture
        JLabel flippedimg = new JLabel(new ImageIcon(imagep));
        // Creates a JPanel to hold the flipped image and then adds it to the cardPanel 
        JPanel FPanel = new JPanel();
        FPanel.add(flippedimg);
        // Adds the flipped panel to the cardpanel
        cardPanel.add(FPanel, "flipped");
        // Resets current image to the flipped image
        currentImage = imagep;
        // Gets the average RGB with getRGB and changes the label
        RGB.setText(getRGB());
        // Displays the flipped card
        cl.show(cardPanel, "flipped");
    }

    /*
     A method that converts the image to Perlin toNoise
     //Credit: https://gist.github.com/Fataho/5b422037a6fdcb21c9134ef34d2fa79a
     */
    public void toNoise() throws IOException {
        // Initialize time to 0
        double time = 0;
        // Make a copy to manipulate with createCopy
        BufferedImage noiseimg = createCopy();
        // Add .01 to time
        time += 0.01;
        // For every pixel in the y dimension
        for (int y = 0; y < noiseimg.getHeight(); y++) {
            // For every pixel in the x dimension
            for (int x = 0; x < noiseimg.getWidth(); x++) {
                // Set dx to the pixel divided by the image width
                double dx = (double) x / noiseimg.getWidth();
                // Set the dy to the pixel divided by the image height
                double dy = (double) y / noiseimg.getHeight();
                // Set frequency to 6
                int frequency = 6;
                // Set noisein to the call of noise with the time, frequency, dx, and dy 
                double noisein = noise((dx * frequency) + time, (dy * frequency) + time);
                // Set noisein to noisein minus 1 divided by 2
                noisein = (noisein - 1) / 2;
                // Set b to noisein times 225
                int b = (int) (noisein * 0xFF);
                // Set r to b times 65536
                int r = b * 0x10000;
                // Set final value to r
                int finalValue = r;
                // set the RGB value of the image pixel to the final value
                noiseimg.setRGB(x, y, finalValue);
            }
        }
        // Create a new label with an imageIcon holding the toNoise image
        // Credit: https://stackoverflow.com/questions/299495/how-to-add-an-image-to-a-jpanel#:~:text=Try%20this%3A,myPicture
        JLabel noiselabel = new JLabel(new ImageIcon(noiseimg));
        // Create a new panel and add the noiselabel to it
        JPanel IPanel = new JPanel();
        IPanel.add(noiselabel);
        // Add the panel to the cardPanel
        cardPanel.add(IPanel, "noise");
        // Reset currentImage to noiseimg
        currentImage = noiseimg;
        // Recalcalculate the RGB values
        RGB.setText(getRGB());
        // Display the toNoise card
        cl.show(cardPanel, "noise");
    }

    /*
     Used in the perlin noise calculations
     @Return a double 
    Credit: https://gist.github.com/Fataho/5b422037a6fdcb21c9134ef34d2fa79a
     */
    public double noise(double x, double y) {
        // Set xi to the floor of x and 255
        int xi = (int) Math.floor(x) & 255;
        // Set yi to the floor of y and 255
        int yi = (int) Math.floor(y) & 255;
        // Set g1 to the permutaion of xi and yi
        int g1 = p[p[xi] + yi];
        // Set g2 to the permutation of xi plus 1 and yi
        int g2 = p[p[xi + 1] + yi];
        // Set g3 to the permutation of xi plus 1 plus yi
        int g3 = p[p[xi] + yi + 1];
        // Set g4 to the permutation of xi plus 1 and yi plus 1
        int g4 = p[p[xi + 1] + yi + 1];

        // Set xf to x minus the floor of x
        double xf = x - Math.floor(x);
        // Set yf to y minus the floor of y
        double yf = y - Math.floor(y);

        // Set d1 to grad using g1, xf, and yf
        double d1 = grad(g1, xf, yf);
        // Set d2 to grad using g2, xf minus1, and yf
        double d2 = grad(g2, xf - 1, yf);
        // Set d3 to grad using g3, xf, and yf minus 1
        double d3 = grad(g3, xf, yf - 1);
        // Set d4 to grad using g4, xf minus 1, and yf minus 1
        double d4 = grad(g4, xf - 1, yf - 1);

        // Set u to xf times xf times xf times xf times xf times 6 minus 15 plus 10 
        double u = xf * xf * xf * (xf * (xf * 6 - 15) + 10);
        // Set v to yf times yf times yf times yf times yf times 6 minus 15 plus 10 
        double v = yf * yf * yf * (yf * (yf * 6 - 15) + 10);

        // Set x1Inter to 1 minus u times d1 plus u times d2
        double x1Inter = ((1 - u) * d1 + u * d2);
        // Set x2Inter to 1 minus u times d3 plus u times d4
        double x2Inter = ((1 - u) * d3 + u * d4);
        // Set yInter to 1 minus v times z1Inter plus v times x2Inter 
        double yInter = ((1 - v) * x1Inter + v * x2Inter);

        // Return yInter
        return yInter;

    }

    /*
     A method that takes an int hash and two doubles, used to calcualte perlin noise
     @Return double the desired number
    Credit: https://gist.github.com/Fataho/5b422037a6fdcb21c9134ef34d2fa79a
     */
    public double grad(int hash, double x, double y) {
        // Switch statement for hash for cases 0-3
        return switch (hash & 3) {
            // Return x plus y
            case 0 ->
                x + y;
            // Return -x plus y
            case 1 ->
                -x + y;
            // Return x minus y
            case 2 ->
                x - y;
            // Return -x minus y
            case 3 ->
                -x - y;
            // Default return 0
            default ->
                0;
        };
    }

    /*
     A list of permutations used to make perlin noise
    Credit: https://gist.github.com/Fataho/5b422037a6fdcb21c9134ef34d2fa79a
     */
    public int p[] = new int[512], permutation[] = {151, 160, 137, 91, 90, 15,
        131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23,
        190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33,
        88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166,
        77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244,
        102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196,
        135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123,
        5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42,
        223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9,
        129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228,
        251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107,
        49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254,
        138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180
    };

    {
        // For 256 times
        // Credit: https://gist.github.com/Fataho/5b422037a6fdcb21c9134ef34d2fa79a
        for (int i = 0; i < 256; i++) {
            // Set the permutation at 256 plus i to the permutation at i 
            p[256 + i] = p[i] = permutation[i];
        }

    }

    // Main Method
    public static void main(String[] args) throws IOException {

        // Creating Object of Main class.
        Main cl = new Main();

        // Function to set default operation of JFrame.
        cl.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Function to set visibility of JFrame.
        cl.setVisible(true);
    }
}
