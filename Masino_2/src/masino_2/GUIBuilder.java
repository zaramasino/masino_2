/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package masino_2;

/**
 *
 * @author zara
 */
import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.filechooser.FileSystemView;
// Inheriting from JFrame allows polymorphism
// to be used to display the window

public class GUIBuilder extends JFrame {
// Create a " label " , just some text in the GUI

    private JLabel label1;
// Constructor lays out the GUI entirely

    public GUIBuilder() throws IOException {
        super(" JLabel Demo "); // The name of the window
        setLayout(new FlowLayout()); // A type of layout
// JLabel constructor with string argument
        label1 = new JLabel(" Here ’s a JLabel ");
// Mouse - over text
        label1.setToolTipText(" This is label1 ");
        add(label1); // Add the label to the JFrame

        JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());

        int returnValue = jfc.showOpenDialog(null);
        // int returnValue = jfc.showSaveDialog(null);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File selectedFile = jfc.getSelectedFile();
            BufferedImage myPicture = ImageIO.read(new File(""));
            JLabel picLabel = new JLabel(new ImageIcon(myPicture));
            add(picLabel);
            System.out.println(selectedFile.getAbsolutePath());
        }
    } // end of GUIDemo
} // end of GUIDemo

/*
import java.io.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.filechooser.*;
class filechooser extends JFrame implements ActionListener {
 
    // Jlabel to show the files user selects
    static JLabel l;
 
    // a default constructor
    filechooser()
    {
    }
 
    
    public void actionPerformed(ActionEvent evt)
    {
        // if the user presses the save button show the save dialog
        String com = evt.getActionCommand();
 
        if (com.equals("save")) {
            // create an object of JFileChooser class
            JFileChooser j = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
 
            // invoke the showsSaveDialog function to show the save dialog
            int r = j.showSaveDialog(null);
 
            // if the user selects a file
            if (r == JFileChooser.APPROVE_OPTION)
 
            {
                // set the label to the path of the selected file
                l.setText(j.getSelectedFile().getAbsolutePath());
            }
            // if the user cancelled the operation
            else
                l.setText("the user cancelled the operation");
        }
 
        // if the user presses the open dialog show the open dialog
        else {
            // create an object of JFileChooser class
            JFileChooser j = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
 
            // invoke the showsOpenDialog function to show the save dialog
            int r = j.showOpenDialog(null);
 
            // if the user selects a file
            if (r == JFileChooser.APPROVE_OPTION)
 
            {
                // set the label to the path of the selected file
                l.setText(j.getSelectedFile().getAbsolutePath());
            }
            // if the user cancelled the operation
            else
                l.setText("the user cancelled the operation");
        }
    }
}*/
